package class02.point;

/**
 * @author zl
 */
public class Point {
    float x;
    float y;
    float perimeter;

    public float getPerimeter() {
        return perimeter;
    }

    public Point(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public Point() {
    }

    public float rectangle(Point m, Point n) {
        perimeter = 2 * (m.x - n.x + m.y - n.y);
        return (m.x - n.x) * (m.y - n.y);
    }
}
