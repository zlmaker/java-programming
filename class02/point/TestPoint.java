package class02.point;

import class02.point.Point;

import java.util.Scanner;

/**
 * @author zl
 */
public class TestPoint {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Point point = new Point();
        System.out.println("矩形面积:"+
                point.rectangle(new Point(scanner.nextFloat(), scanner.nextFloat()),new Point(scanner.nextFloat(), scanner.nextFloat())));
        System.out.println("矩形周长:"+
                point.getPerimeter());
    }
}
