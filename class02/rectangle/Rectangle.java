package class02.rectangle;

/**
 * @author zl
 */
public class Rectangle {
        float cx;
        float cy;
        float bx;
        float by;

    public Rectangle(float bx, float by, float cx, float cy) {
        this.bx = bx;
        this.by = by;
        this.cx = cx;
        this.cy = cy;
    }

    public float area() {
        return Math.abs((bx - cx) * (by - cy));
    }

    public float perimeter() {
        return Math.abs(2 * (bx - cx + by - cy));
    }
}
