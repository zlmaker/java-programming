package class02.tree;

/**
 * @author zl
 */
public class Tree {
    private int age;

    public Tree(int years) {
        age = years;
    }

    public void grow(int year) {
        age = age + year;
    }

    public int age() {
        return age;
    }
}
