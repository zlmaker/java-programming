package class02.counter;
/**
 * @author zl
 */
public class Counter {
    private int count;
    private int maxValue;
    public Counter(int i, int i1) {
        count = i;
        maxValue = i1;
    }
    public void increment() {
        if (count>=maxValue){
            System.out.println("已经是最大值了");
        }else {
            count++;
        }
    }
    public void decrement() {
        if (count<=0){
            System.out.println("计数器不为负数");
        }else {
            count--;
        }
    }
    public void setMaxValue(double v) {
        if (v>Integer.MAX_VALUE){
            System.out.println("超过int的最大值");
        }else {
            maxValue= (int) v;
        }
    }
    public void setCount(int i) {
        if (i>maxValue){
            System.out.println("超过设置的最大值");
        }else {
            count = i;
        }
    }
    public int getCount() {
        return count;
    }
    public int getMaxValue() {
        return maxValue;
    }
}
