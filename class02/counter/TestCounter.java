package class02.counter;

/**
 * @author zl
 */
public class TestCounter {
    public static void main(String[] args) {
        //计数器初始值为1，最大值为100
        Counter c = new Counter(1, 100);
        //计数器递增，超出最大值报错
        for (int i = 1; i <= 103; i++) {
            c.increment();
        }
        System.out.println("当前值=" + c.getCount());
        //计数器递减，超出最小值报错
        for (int i = 1; i <= 102; i++) {
            c.decrement();
        }
        System.out.println("当前值=" + c.getCount());
        //设置计数器的当前值和最大值
        c.setCount(150);
        c.setMaxValue(Integer.MAX_VALUE + 100.0);
        c.setMaxValue(200);
        System.out.println("当前值=" + c.getCount());
        System.out.println("最大值=" + c.getMaxValue());

    }
}
