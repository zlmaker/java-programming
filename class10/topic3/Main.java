package class10.topic3;

import java.io.*;
import java.util.Scanner;

/**
 * @author zl
 */
public class Main {
    public static void main(String[] args) throws IOException {
        String buffer;
        Scanner scanner = new Scanner(System.in);
        BufferedReader reader1 = null;
        BufferedReader reader2 = null;
        BufferedWriter writer = null;
        try {
            System.out.println("请输入第一个文件名:");
            reader1 = new BufferedReader(new FileReader(scanner.nextLine()));
            System.out.println("请输入第二个文件名:");
            reader2 = new BufferedReader(new FileReader(scanner.nextLine()));
            writer = new BufferedWriter(new FileWriter("mergeFiles.txt", true));
            while ((buffer = reader1.readLine()) != null) {
                writer.write(buffer);
                writer.newLine();
            }
            while ((buffer = reader2.readLine()) != null) {
                writer.write(buffer);
                writer.newLine();
            }
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader1 != null) {
                reader1.close();
            }
            if (reader2 != null) {
                reader2.close();
            }
            if (writer != null) {
                writer.close();
            }
        }
    }
}
