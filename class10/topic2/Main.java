package class10.topic2;

import java.io.*;
import java.util.Scanner;

/**
 * @author zl
 */
public class Main {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        File srcFile = new File(scanner.nextLine());
        String s = srcFile.getParent();
        FileInputStream fis = null;
        FileOutputStream fos = null;
        try {
            if (srcFile.exists()) {
                File dstFile = new File(s, "copy_data.txt");
                fis = new FileInputStream(srcFile);
                fos = new FileOutputStream(dstFile);
                int len;
                byte[] buffer = new byte[(int) srcFile.length()];
                while ((len = fis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                fos.flush();
            } else {
                boolean newFile = srcFile.createNewFile();
                System.out.println(newFile);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                fis.close();
            }
            if (fos != null) {
                fos.close();
            }
        }
    }
}
