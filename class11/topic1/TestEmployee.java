package class11.topic1;

import java.util.*;

/**
 * @ClassName TestEmployee
 * @Description 测试员工类
 * @Author zl
 * @Date 2021/6/1 18:34
 **/
public class TestEmployee {
    public static void main(String[] args) {
        ArrayList<Employee> employees = new ArrayList<>();
        employees.add(new Employee(1001, "Tom", "Market", 1200));
        employees.add(new Employee(1002, "Jack", "Department", 1300));
        employees.add(new Employee(1003, "Simth", "Market", 200));
        employees.add(new Employee(1004, "Tony", "Department", 300));
        for (Employee employee : employees) {
            System.out.println(employee.getName());
        }
        System.out.println("---------------------");
        for (Employee employee : employees) {
            System.out.println(employee.getName() + ": " + employee.getWage());
        }
        System.out.println("---------------------");
        //        Employee employee1 = employees.stream().filter(employee -> "Jack".equals(employee.getName())).findFirst().get();
        //        System.out.println(employee1.getName());
        for (Employee employee : employees) {
            if ("Jack".equals(employee.getName())) {
                System.out.println("改前：" + employee.getWage());
                employee.changePay(1500);
                System.out.println("改后：" + employee.getWage());
            }
        }
        System.out.println("---------------------");
        //额(⊙﹏⊙)没有小于1000的，那个自己改了两个工资。
        for (Employee employee : employees) {
            if (employee.getWage() < 1000) {
                System.out.println("改前：" + employee.getWage());
                employee.changePay(0.20f);
                System.out.println("改后：" + employee.getWage());
            }
        }
        System.out.println("---------------------");
        float sumOfMarket = 0;
        int countOfMarket = 0;
        float sumOfDepartment = 0;
        int countOfDepartment = 0;
        for (Employee employee : employees) {
            if ("Market".equals(employee.getDepartment())) {
                sumOfMarket += employee.getWage();
                countOfMarket++;
            }
            if ("Department".equals(employee.getDepartment())) {
                sumOfDepartment += employee.getWage();
                countOfDepartment++;
            }
        }
        System.out.println("Market的平均工资：" + sumOfMarket / countOfMarket);
        System.out.println("Department的平均工资：" + sumOfDepartment / countOfDepartment);
        System.out.println("---------------------");
        boolean b = employees.removeIf(employee -> "Tom".equals(employee.getName()));
        System.out.println(b);

    }
}
