package class04;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author zl
 */
public class Test {
    public static void main(String[] args) {
        new class03.test.Test() {
            @Override
            public double distance() {
                return super.distance();
            }

            @Override
            public void printPoint() {
                super.printPoint();
            }
        };
        try (Scanner scanner = new Scanner(System.in)) {
            int a = scanner.nextInt();
            System.out.println(a);
        }
    }
}
