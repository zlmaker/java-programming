package class06.Car;

class Car {
    int carNumber;

    void setNumber(int carNum) {
        this.carNumber = carNum;
    }

    void showNumber() {
        System.out.println("My car No. is :" + carNumber);
    }
}

class TrashCar
        extends Car {
    int capacity;

    void setCapacity(int r) {
        capacity = r;
    }

    void showCapacity() {
        System.out.println("My capacity is:" + capacity);
    }
}

class CarDemo {
    public static void main(String[] args) {
        var democar = new Car();
        democar.setNumber(3838);
        democar.showNumber();
        var demoTrashCar = new TrashCar();
        demoTrashCar.setNumber(4949);
        demoTrashCar.setCapacity(20);
        demoTrashCar.showNumber();
        demoTrashCar.showCapacity();
    }
}
