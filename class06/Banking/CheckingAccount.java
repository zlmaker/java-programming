package class06.Banking;

/**
 * @author zl
 */
public class CheckingAccount
        extends Account {
    private double overdraftProtection;//透支额度

    public CheckingAccount(double balance) {
        super(balance);
    }

    public CheckingAccount(double balance, double protect) {
        super(balance);
        this.overdraftProtection = protect;
    }

    @Override
    public boolean withdraw(double amt) {
        if (amt<=balance){
            balance=balance-amt;
            return true;
        }else if(overdraftProtection<0||amt>overdraftProtection){
            return false;
        }else {
            balance=0;
            overdraftProtection=overdraftProtection-(amt-balance);
            return true;
        }
    }
}
