package class08.topic3;

/**
 * @author zl
 */
public class Books {
    private String bookName;
    private int bookId;
    private double bookPrice;
    private static int bookCount=0;

    public Books(String bookName, double bookPrice) {
        this.bookName = bookName;
        this.bookPrice = bookPrice;
        bookCount++;
        this.bookId=bookCount;
    }

    public String getBookName() {
        return bookName;
    }

    public int getBookId() {
        return bookId;
    }

    public double getBookPrice() {
        return bookPrice;
    }

    public static int getBookCount() {
        return bookCount;
    }
}
