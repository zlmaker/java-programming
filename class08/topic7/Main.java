package class08.topic7;

import class07.circle.Circle;

import java.util.Arrays;

/**
 * @author zl
 */
public class Main {
    public static void main(String[] args) {
        Circle[] circles = {new Circle(2), new Circle(10), new Circle(8), new Circle(4), new Circle(12), new Circle(20), new Circle(18)};
        System.out.println(Arrays.toString(circles));
        Arrays.fill(circles, new Circle(30));
        System.out.println(Arrays.toString(circles));
        Arrays.fill(circles, 3, 5, new Circle(50));
        System.out.println(Arrays.toString(circles));
        //deepToString是深层次的遍历打印，适合多维数组
        System.out.println(Arrays.deepToString(circles));
        Circle[] circles1 = Arrays.copyOf(circles, circles.length);
        System.out.println(Arrays.toString(circles1));
        Circle[] circles2 = Arrays.copyOfRange(circles, 3, 6);
        System.out.println(Arrays.toString(circles2));
        System.out.println(Arrays.equals(circles1, circles2));
        //deepEquals是深层次比较两个数组，适合多维数组
        System.out.println(Arrays.deepEquals(circles1, circles2));
        Arrays.sort(circles);
        System.out.println(Arrays.toString(circles));
        System.out.println(Arrays.binarySearch(circles, new Circle(50)));
    }
}
