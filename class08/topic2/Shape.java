package class08.topic2;

/**
 * @author zl
 */
public abstract class Shape {
    static final double PI=3.1415926;
    abstract double area();
    abstract double perimeter();
}
