package class08.topic2;

import java.util.Scanner;

/**
 * @author zl
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Oval oval = new Oval(scanner.nextDouble(),scanner.nextDouble());
        System.out.println("The area of "+oval+" is "+oval.area());
        System.out.println("The perimeterof "+oval+" is "+oval.perimeter());
    }
}
