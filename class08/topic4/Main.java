package class08.topic4;

import java.util.Scanner;

/**
 * @author zl
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Goods goods = new Goods(scanner.next(), scanner.next(), scanner.nextDouble());
        System.out.println(goods);
    }
}
