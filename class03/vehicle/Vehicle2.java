package class03.vehicle;

/**
 * @author zl
 */
public class Vehicle2
        extends Vehicle1 {
    public Vehicle2(double maxLoad) {
        super(maxLoad);
    }

    private double newtsToKilo(double weight) {
        return (weight / 9.8);
    }

    private double kiloToNewts(double weight) {
        return (weight * 9.8);
    }
}
