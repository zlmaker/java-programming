package class03.vehicle;

/**
 * @author zl
 */
public class Vehicle {

    public double load;
    public double maxLoad;
    public Vehicle(double v) {
        maxLoad=v;
    }

    public double getLoad() {
        return load;
    }

    public double getMaxLoad() {
        return maxLoad;
    }

    public double addBox(double v) {
        load=v+load;
        return load;
    }
}
