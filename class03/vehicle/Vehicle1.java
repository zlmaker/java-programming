package class03.vehicle;

/**
 * @author zl
 */
public class Vehicle1 {
    private double load;
    private double maxLoad;

    public Vehicle1(double maxLoad) {
        this.maxLoad = maxLoad;
    }

    public double getLoad() {
        return load;
    }

    public double getMaxLoad() {
        return maxLoad;
    }
    public boolean addBox(double weight){
        if (weight+load>maxLoad){
            return false;
        }else {
            load = load + weight;
            return true;
        }
    }
}
