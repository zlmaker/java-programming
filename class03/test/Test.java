package class03.test;

import java.util.Scanner;

/**
 * @author zl
 */
public class Test {
    public static void main(String[] args) {
        Test p1 = new Test(2, 3);
        p1.x = 100;
        p1.y = 200;
    }

    public static double Pi = 3.14;
    //属性、成员变量

    private int x;
    private int y;

    //构造函数，每个类都有至少一个（用户没有定义，编译器自动添加无参空构造函数）
    //函数名和类名相同、无返回值、函数调用使用new、作用是对属性进行初始化
    //允许函数重载:函数名相同、参数个数或类型不同
    //this用法1：当前对象

    public Test(int x, int y) {
        this.x = x;
        this.y = y;
    }

    //this用法2：this()指代构造函数

    public Test(int a) {
        this(a, a);
    }

    public Test() {
        Scanner sc = new Scanner(System.in);
        x = sc.nextInt();
        y = sc.nextInt();
    }

    //方法、成员函数

    public void printPoint() {
        System.out.println("(" + this.x + "," + this.y + ")");
    }

    public double distance() {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }
}
