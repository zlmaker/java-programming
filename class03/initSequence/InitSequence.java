package class03.initSequence;

/**
 * @author zl
 */
public class InitSequence {
    Tools ts1=new Tools();
    {
        System.out.println("Hello");
    }
    InitSequence(){
        System.out.println("InitSequence");
    }
    public static void main(String[] args) {
        System.out.println("call Tools.f(4) in main");
        Tools.t4.f(4);
        System.out.println("Creating new Tools() in main");
        new Tools();
        System.out.println("Creating new InitSequence() in main");
        new InitSequence();
    }
    static Tools ts2=new Tools();
}
class Tools{
    Tool t1=new Tool(1);
    static Tool t3=new Tool(3);
    static {
        System.out.println("进入静态初始化块");
        t3=new Tool(33);
        t4=new Tool(44);
        System.out.println("退出静态初始化块");
    }
    {
        System.out.println("进入实例初始化块");
        t1=new Tool(11);
        t2=new Tool(22);
        System.out.println("退出实例初始化块");
    }
    Tools(){
        System.out.println("Tools()");
        t2=new Tool(222);
    }
    static Tool t4=new Tool(4);
    Tool t2=new Tool(2);
}
class Tool{
    Tool(int i){
        System.out.println("Tool("+i+")");
    }
    void f(int i) {
        System.out.println("f("+i+")");
    }
}
/*
Tool(3)
进入静态初始化块
Tool(33)
Tool(44)
退出静态初始化块
Tool(4)
Tool(1)
进入实例初始化块
Tool(11)
Tool(22)
退出实例初始化块
Tool(2)
Tools()
Tool(222)
call Tools.f(4) in main
f(4)
Creating new Tools() in main
Tool(1)
进入实例初始化块
Tool(11)
Tool(22)
退出实例初始化块
Tool(2)
Tools()
Tool(222)
Creating new InitSequence() in main
Tool(1)
进入实例初始化块
Tool(11)
Tool(22)
退出实例初始化块
Tool(2)
Tools()
Tool(222)
Hello
InitSequence
*/
