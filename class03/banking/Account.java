package class03.banking;

/**
 * @author zl
 */
public class Account {
    private double balance;

    public Account(double v) {
        balance=v;
    }

    public void withdraw(double v) {
        balance=balance-v;
    }

    public void deposit(double v) {
        balance=balance+v;
    }

    public double getBalance() {
        return balance;
    }
}
