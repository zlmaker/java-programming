package class05.animal;

interface Pet {
    public String getName();

    public void setName(String name);

    public void play();
}

