package class05.animal;

public class Fish
        extends Animal
        implements Pet {
    public Fish() {
        super(0);
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public void setName(String name) {

    }

    @Override
    public void play() {
        System.out.println("Fish swim in their tanks all day.");
    }

    @Override
    public void walk() {
        super.walk();
        System.out.println("Fish, of course, can't walk; they swim.");
    }

    @Override
    public void eat() {

    }
}
