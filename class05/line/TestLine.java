package class05.line;

import java.util.Scanner;

/**
 * @author zl
 */
public class TestLine {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Line line = new Line(new Point(1, 2), new Point(26, 49));
        System.out.println("长度:" + line.getLength());
        System.out.println("斜率:" + line.getSlope());
        Line line1 = new Line(new Point(scanner.nextInt(), scanner.nextInt()), new Point(scanner.nextInt(), scanner.nextInt()));
        System.out.println("长度:" + line1.getLength());
        System.out.println("斜率:" + line1.getSlope());
    }
}



