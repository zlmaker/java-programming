package class09.topic1;

/**
 * @author zl
 */
public class TestExceptions {
    public static void main(String[] args) {
        try {
            for (int i = 0; true; i++) {
                System.out.println("args[" + i + "] is '" + args[i] + "'");
            }
        } catch (Exception e) {
            System.out.println("Exception caught: " + e);
            System.out.println("Quiting...");
        }
    }
}
