package class09.topic2;

/**
 * @author zl
 */
public class EmailAddressException
        extends Exception {
    private String message;

    public EmailAddressException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
