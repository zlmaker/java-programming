package class09.topic2;

import java.util.Scanner;

/**
 * @author zl
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        try {
            checkEmail(str);
        } catch (EmailAddressException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void checkEmail(String s) throws EmailAddressException {
        if (!s.contains("@")) {
            throw new EmailAddressException("邮件地址中没有”@”符号");
        } else if (s.substring(0, s.indexOf("@")).length() < 6) {
            throw new EmailAddressException("邮件地址中的“@“符号之前的字符个数至少为6位");
        }
    }

}

