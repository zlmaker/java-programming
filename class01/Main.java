package class01;


import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * @author zl
 */
public class Main {
    public static void main(String[] args) {
        /**
         * i表示输入的数
         * j表示余数
         */
        Scanner scanner = new Scanner(System.in);
        int i = scanner.nextInt();
        int j;
        /**
         * 使用集合
         */
        ArrayList<Integer> arrayList = new ArrayList<>();
        while (i!=0){
            j=i%10;
            arrayList.add(j);
            i=i/10;
        }
        /**
         * 使用reverse类进行排序
         */
        Collections.reverse(arrayList);

        for (int i1:arrayList){
            System.out.printf("%d ",i1);
        }
    }
}
